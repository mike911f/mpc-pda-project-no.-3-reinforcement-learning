package qlearning;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import robocode.RobocodeFileOutputStream;

//=========================================================================================
//  ROBOCODE - Qlearning
//  14.12.2020
//  Michael Jurek, Monika B�hmov�, Alexandra Kuzmina, Jakub Mich�lek, Martin Stra�evsk�
//=========================================================================================


public class Q {
	
	public static final double ALPHA = 0.1;						// koeficient rychlosti uceni
	public static final double GAMMA = 0.9;						// koeficient znehodnoceni
	
	public static final int numStates = States.numStates;
	public static final int numActions = States.numActions;
	
	private double data[][];
	
	public Q() {
		data = new double[numStates][numActions];
		initialize();
	}
	
	public void initialize() {
		for(int i =0; i<numStates;i++) {
			for(int j=0;j<numActions;j++) {
				data[i][j] = 0.0;
			}
		}
	}
	
	double getQ(int state, int action) {
		return data[state][action];
	}
	
	void setQ(int state, int action, double q_value) {
		data[state][action] = q_value;
	}
	
	public int actionOfMaxQ(int state) {					// vyber akce s nejvetsim uzitkem pro dany stav
		int action = 0;
		double max = -Double.MAX_VALUE;
		for(int i=0;i<numActions;i++) {
			if(getQ(state,i)>max) {
				max = getQ(state,i);
				action = i;
			}
		}
		return action;
	}
	
	double maxQ(int state) {								// Q hodnota provedeni akce s nejvetsim uzitkem
		double max = -Double.MAX_VALUE;
		for(int i=0;i<numActions;i++) {
			if(getQ(state,i)>max) {
				max = getQ(state,i);
			}
		}
		return max;
	}
	
	// Q[S,A] = (1-ALPHA)*oldQ(S,A) + ALPHA*(reward(S,A)+GAMMA*maxQ(S',B))
	public void QLearning(int prevState, int prevAction, int state, int reward) {	// zpetnovazebne uceni
		double oldQ = getQ(prevState,prevAction);
		double maxQ = maxQ(state);
		double newQ = oldQ + ALPHA*(reward+GAMMA*maxQ-oldQ);
		setQ(prevState,prevAction,newQ);
	}

	public void saveData(File dataFile) {											// ukladani Q hodnot do souboru
		PrintStream w = null;
		try {
			w = new PrintStream(new RobocodeFileOutputStream(dataFile));
			for (int i = 0; i < States.numStates; i++) {
				for (int j = 0; j < States.numActions; j++) {
					w.print(String.valueOf(data[i][j])+" ");
				}
				w.println();
			}
		}catch(IOException e) {
			System.out.println("IOException trying to write to file: "+e);
		}finally {
			try {
				if(w!=null) {
					w.close();
				}
			}catch(Exception e) {
				System.out.println("Exception trying to close writer: "+e);
			}
		}
	}
}
