package qlearning;

//=========================================================================================
//  ROBOCODE - Qlearning
//  14.12.2020
//  Michael Jurek, Monika B�hmov�, Alexandra Kuzmina, Jakub Mich�lek, Martin Stra�evsk�
//=========================================================================================


public class States {
	static final int numEnergy = 5;										// pocet kvantovanych hodnot pro energii
	static final int numDistance = 9;									// pocet kvantovanych hodnot pro vzdalenost
	static final int numGunHeat = 2;									// pocet kvantovanych hodnot teplotu zbrane
	static final int numBeraings = 10;									
	
	public static int numStates = numEnergy * numDistance * numGunHeat;
	public static int numActions = 5;
	
	private static int S[][][];											// tabulka reprezentujici vnitrni stavy prostredi
	
	public int robotEnergy;
	public int robotGunHeat;
	public int robotHeading;
	public int bearing;
	public int distance;
	public int enemyEnergy;
	public double enemyX, enemyY;
	
	public States(){
		S = new int[numEnergy][numDistance][numGunHeat];
		initialize();
	}
	
	private void initialize() {											// linearizace tabulky prostredi a nastaveni pocatecnich hodnot
		int count = 0;
		for(int i=0;i<numEnergy;i++) {
			for(int j=0;j<numDistance;j++) {
				for(int k=0;k<numGunHeat;k++) {
					S[i][j][k] = count++;
				}
			}
		}
		this.robotEnergy = 4;
		this.robotHeading = 0;
		this.robotGunHeat = 0;
		this.bearing = 0;
		this.distance = 10;
	}

	//kvantovane hodnoty												// prepocitani stavu prostredi
	public void update(double energy, double distance, double gunHeat, double enemyX, double enemyY) {
		this.robotEnergy = robotGetEnergy(energy);
		this.distance = robotGetDistance(distance);
		this.robotGunHeat = robotGetGunHeat(gunHeat);
		this.enemyX = enemyX;
		this.enemyY = enemyY;
	}
	
	//==============================================================
	//kvantovane hodnoty
	//==============================================================
		
	public int robotGetEnergy(double energy) {
		if (energy >= 0 && energy <20)
			return 0;
		else if(energy >=20 && energy<40)
			return 1;
		else if(energy >=40 && energy<60)
			return 2;
		else if(energy >=60 && energy<80)
			return 3;
		else
			return 4;
	}
	
	public int robotGetDistance(double distance) {
		if (distance >=0 && distance < 50)
			return 0;
		else if(distance >= 50 && distance < 100)
			return 1;
		else if(distance >= 100 && distance <150)
			return 2;
		else if(distance >=150 && distance < 200)
			return 3;
		else if(distance >= 200 && distance < 250)
			return 4;
		else if(distance >=250 && distance < 300)
			return 5;
		else if(distance >=300 && distance <350)
			return 6;
		else if(distance >= 350 && distance <400)
			return 7;
		else
			return 8;
	}
	
	public int robotGetGunHeat(double gunHeat) {
		if(gunHeat > 0)
			return 1;
		else
			return 0;
	}
	
	public int robotGetHeading(double heading) {
		return (int) (heading/36);
	}
	
	public int robotGetBearing(double bearing) {
		return (int) ((bearing+180)/36);
	}
	
	public int getCurrentState() {
		return S[robotEnergy][distance][robotGunHeat];
	}
	
}
