package sample;

import robocode.AdvancedRobot;
import robocode.BattleEndedEvent;
import robocode.BulletHitEvent;
import robocode.BulletMissedEvent;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import qlearning.States;
import qlearning.Q;
import java.util.Random;


//=========================================================================================
//  ROBOCODE - Qlearning
//  14.12.2020
//  Michael Jurek, Monika B�hmov�, Alexandra Kuzmina, Jakub Mich�lek, Martin Stra�evsk�
//=========================================================================================

public class QRobot extends AdvancedRobot {
	
	static States States = new States();					// prostredi
	static Q Q = new Q();									// hodnoty (uzitek) pro zpetnovazebne uceni
	
	static int prevState=0, state=0, prevAction=0, action=0;
	
	int reward = 0;											// odmena po provedeni akce
	int runs = 0;
	double epsilon = 0.8;									// mira uceni
	double epsilonMin = 0.01;
	double epsilonMax = 1.0;
	double decay_rate = 0.005;								// zpomalovani uceni
	
	private int moveDirection = 1;			// smer pohybu +1/-1
	static boolean firstTurn = true;		

	public void run() {
		setAdjustGunForRobotTurn(true);		// nezavisly pohyb kanonu na pohybu robota
		setAdjustRadarForGunTurn(true);		// nezavisly pohyb radaru na kanonu
		
		while(true) {
			turnRadarRight(45);		
		}
	}	
	
	public void onScannedRobot(ScannedRobotEvent e) {
		double absbearing = (getHeading()+e.getBearing())%360;					// poloha k souperovi
		double enemyX = getX()+Math.sin(absbearing)*e.getDistance();			// souradnice nepritele x,y
		double enemyY = getY()+Math.cos(absbearing)*e.getDistance();
		
		States.update(getEnergy(), e.getDistance(), getGunHeat(),enemyX,enemyY);// prepocita stavy prostredi
		double firePower = Math.min(3, 400/e.getDistance());					// sila strely reaguje na vzdalenost
		double turn = getHeading() - getGunHeading()+e.getBearing();			// smer k nepriteli 
																				// -180<bearing<180
		if (turn > 180)															// uprava souradnic
			turn -= 360;
		if (turn < -180)
			turn += 360;
		
		setTurnGunRight(turn);													// natoci zbran o uhel
		
		runs++;																	// pocita cislo epizody
		if (firstTurn) {															// nahodna akce
			prevState = States.getCurrentState();
			Random randomGenerator = new Random();
			prevAction = randomGenerator.nextInt(qlearning.States.numActions);
			takeAction(prevAction, firePower);
			firstTurn = false;
		}
		else {																
			
			state = States.getCurrentState();									// ziska hodnotu stavu prostredi
			Q.QLearning(prevState, prevAction, state, reward);					// provede uceni
			prevState = state;
			
			if(Math.random() > epsilon) {										// porovnanim nahodne hodnoty a miry uceni vybere akci
				prevAction = Q.actionOfMaxQ(prevState);							// >epsilon - vybere akci z Q tabulky
				takeAction(prevAction, firePower);
			}else {
				Random randomGenerator = new Random();							// <epsilon - vzbere nahodnou akci pro dany stav
				prevAction = randomGenerator.nextInt(qlearning.States.numActions);
				takeAction(prevAction, firePower);
			}
			
		}
		// prechod z exploration do exploitation
		epsilon = epsilonMin + (epsilonMax - epsilonMin)*Math.exp(-decay_rate*runs); // update miry uceni
		setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
		
	}
	
	// =====================================================
	// ACTIONS!
	private void takeAction(int action, double power) {							// zvoli akci
		switch(action) {
		case 0: motion();
			break;
		case 1: turning();
			break;
		case 2: attackTurning(power);
			break;
		case 3: runAway();
			break;
		case 4: fire(power);
			break;
		}
	}
	
	public void motion() {														// pohyb
		if(getVelocity() == 0) {
			moveDirection *= -1;
			ahead(10);
		}
		
		if (moveDirection == -1) { 	// forward
			setAhead(-100);
		}else {
			setAhead(100);			// backward
		}
	}
	
	public void attackTurning(double power) {									// utocny pohyb
		setTurnGunRight(States.bearing);
		fire(power);
	}
	
	public void turning() {														// otaceni
		out.println("Turning of "+States.bearing);
		setTurnRight(States.bearing);

	}
	
	public void runAway() {														// uhybny manevr
		setTurnRight(States.bearing+90);
		setAhead(-100);
	}

	
	// ======================================================
	// LEARNING
	private void updateQ() {													// uceni Q hodnot po zmene odmeny
		state = States.getCurrentState();
		Q.QLearning(prevState, prevAction, state, reward);
		prevState = state;
	}
	
	//=====================================================
	//REWARD FUNCTIONS
	//=====================================================
	
	public void onHitWall(HitWallEvent e) {										// akce pri narazu do zdi
		reward -= 3;
		moveDirection *= -1;
		updateQ();
	}
	
	public void onHitByBullet(HitByBulletEvent e) {								// akce pri vlastnim zasahu
		reward -= (int)(e.getBullet().getPower()*2);
		updateQ();
	}
	
	public void onBulletHit(BulletHitEvent e) {									// akce pri zasahu soupere
		reward += (int)(e.getBullet().getPower()*4);
		updateQ();
	}

	public void onDeath(RobotDeathEvent e) {									// akce pri smrti robota
		reward -= 10;
		updateQ();
	}
	
	public void onWin(WinEvent e) {												// akce pri vyhre
		reward += 10;
		updateQ();
	}
	
	public void onHitRobot(HitRobotEvent e) {									// akce pri zasahu nepritele
		reward -= 3;
		moveDirection *= -1;
		updateQ();
	}
	
	public void onBulletMissedEvent(BulletMissedEvent e) {						// akce pri minuti strely
		reward -= (int)(e.getBullet().getPower());
		updateQ();
	}

	public void onBattleEnded(BattleEndedEvent e) {								// akce pri ukonceni bitvy
		saveData();
	}
	
	public void saveData() {
		try {
			Q.saveData(getDataFile("QData.txt"));
		}catch(Exception e) {
			out.println("Exception trying to write: "+e);
		}
	}
	
	
	
}
