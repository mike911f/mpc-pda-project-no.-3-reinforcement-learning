package sample;

import java.awt.Color;
import java.util.Random;

import robocode.AdvancedRobot;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;

public class RandomRobot extends AdvancedRobot{
		
	public void run() {
		setBodyColor(new Color(0, 200, 0));
		setGunColor(new Color(0, 150, 50));
		setRadarColor(new Color(0, 100, 100));
		setBulletColor(new Color(255, 255, 100));
		setScanColor(new Color(255, 200, 200));

		Random rand = new Random();
		while(true) {
			ahead(rand.nextInt(1000));
			turnGunRight(360);
			back(rand.nextInt(500));
			turnGunRight(360);
		}
	}
	
	public void onScannedRobot(ScannedRobotEvent e){
	    double distance = e.getDistance(); 
	    if(distance > 800)
	        fire(5);
	    else if(distance > 600 && distance <= 800)
	        fire(4);
	    else if(distance > 400 && distance <= 600)
	        fire(3);
	    else if(distance > 200 && distance <= 400)
	        fire(2);
	    else if(distance < 200)
	        fire(1);
	}
	
	public void onHitWall(HitWallEvent e) {
		double wallDist = e.getBearing();
		turnRight(-wallDist);
		ahead(100);
	}
}
